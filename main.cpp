#include <iostream>

void repeat(const char *string, int times) {
    for (int i = 0; i < times; ++i) std::cout << string;
}

int main(int argc, char **argv) {
    if(argc != 2) std::__throw_runtime_error("Please specify the tree's height as the first argument");

    int totalHeight = std::stoi(std::string(argv[1])); //totalHeight from first argument
    int width = totalHeight*2 - 1; //force width to make a nice tree
    int boxHeight = totalHeight * 0.15; //15% of the height is the stump and box.

    srand (time(NULL)); //rand() will now give different results...

    for (int row = 0; row < totalHeight - boxHeight; ++row) { //just tree rows, none yet for the box
        int spaces = (width / 2) - row;
        repeat(" ", spaces);
        std::cout << (row == 0 ? "^" : "/"); //top or near edge.

        if (row == totalHeight - boxHeight - 1) repeat("_", width - spaces*2 - 2); //minus sides and edges
        else { //for each position, randomly allocate a light or a bauble.
            for (int column = spaces + 1; column < width - spaces - 1; ++column) {
                int r = rand() % 100;
                if (r < 10) std::cout << "*"; //10% chance of a light
                else if (r < 15) std::cout << "o"; //5% chance of a bauble
                else std::cout << " "; //85% chance of an empty bit of tree
            }
        }

        if (row != 0) std::cout << "\\"; //far edge, if there is one.
        std::cout << std::endl;
    }

    int boxWidth = width * 0.2;
    for(int row = 0; row < boxHeight; ++row){
        repeat(" ", (width - boxWidth)/2);
        std::cout << "|"; //near edge
        repeat(row == boxHeight -1 ? "_" : " ", boxWidth-2); //minus near and far edges.
        std::cout << "|" << std::endl; //far edge
    }

    return 0;
}