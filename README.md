# README #

Christmas tree and base box with flexible height.

### How do I get set up? ###

* There's an IDEA project here, but I used CLion to make it.
    gcc main.cpp #also works.
* Height of the tree is specified as the only command line parameter

### Limitations ###

* I would normally have modularised a bit more, but didn't want to complicate it.
* It would be nice to also specify the width and use the ratio between them to deduce the angle of the edges.
* It would be nice to have used curses to make the lights twinkel!